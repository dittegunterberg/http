<?php
// "HTTP/1.0 404 Not Found"
function status_header($code = 200) {
    $messages = [
        100 => "Continue",
        101 => "Switching Protocols",
        200 => "OK",
        201 => "Created",
        202 => "Accepted",
        203 => "Non-Authoritative Information",
        204 => "No Content",
        205 => "Reset Content",
        206 => "Partial Content",
        300 => "Multiple Choices",
        301 => "Moved Permanently",
        302 => "Moved Temporarily (HTTP/1.0)",
        302 => "Found (HTTP/1.1)",
        303 => "See Other (HTTP/1.1)",
        304 => "Not Modified",
        305 => "Use Proxy",
        307 => "Temporary redirect",
        308 => "Permanent Redirect",
        400 => "Bad Request",
        401 => "Unauthorized",
        402 => "Payment Required",
        403 => "Forbidden",
        404 => "Not Found",
        405 => "Method Not Allowed",
        406 => "Not Acceptable",
        407 => "Proxy Authentication Required",
        408 => "Request Timeout",
        409 => "Conflict",
        410 => "Gone",
        411 => "Length Required",
        412 => "Precondition Failed",
        413 => "Payload Too Large (RFC 7231)",
        414 => "Request-URI Too Long",
        415 => "Unsupported Media Type",
        416 => "Requested Range Not Satisfiable",
        417 => "Expectation Failed",
        500 => "Internal Server Error",
        501 => "Not Implemented",
        502 => "Bad Gateway",
        503 => "Service Unavailable",
        504 => "Gateway Timeout",
        505 => "HTTP Version Not Supported",
        509 => "Bandwidth Limit Exceeded"
    ];

    header("HTTP/1.0 ".$code." ".$messages[$code]);
}

// [header => värde]
// Connection: keep-alive
// header("Connection: keep-alive");
function headers(array $headers = []) {
    foreach ($headers as $header => $value) {
        header($header . " " . $value);  
    }
}

headers([
    "Connection:" => "keep-alive",
    "Hej:" => "Hejsan",
    "Lorum:" => "Ipsum"
]);

function redirect($url, $code = 302) {
    status_header($code);
    header("Location:" . $url);
}